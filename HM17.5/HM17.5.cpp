﻿#include <iostream>

class Vector
{
public:

	Vector() : x(10.1), y(8.5), z(3.7)
	{}
	
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

		int GetY()
	{
		return y;

	}

	double Show()
	{
		double V = x * x + y * y + z * z;

		return sqrt(V);
	
	}

private:

	double x = 0;
	double y = 0;
	double z = 0;

};

int main()
{
	Vector V;
	V.Show();

	std::cout << V.Show();
}
